\chapter{Burns}
\label{ch:burns}

\epigraph{Fire is catching! And if we burn, you burn with us!}
         {Katniss Everdeen, \textit{Mockingjay}}

\index{burns|(}

\noindent
Fire is a beloved tool of protesters worldwide, but it is an unpredictable weapon, and flames can injure those attempting to use it.
Shrapnel from riot control devices can burn protesters, but not all burns are so dramatic.
Hot liquids handed out at solidarity kitchens can burn someone when spilled.
During an all-day action, protesters can get severe sunburns.
Chemical burns and electrical burns, though less common, are still possible depending on the nature of the action and opposition.

The basic physiology of the skin discussed in \fullref{ch:wound_management} is considered a prerequisite for this chapter.

\section*{Types}

There are four main types of burns: thermal, chemical, electrical, and radiation.

\subsection*{Thermal Burns}
\index{burns!thermal|(}

Thermal burns are burns caused by contact with hot objects such as open flame, heated metal, or steam.
The length of time needed to cause a burn depends on the source of heat and duration of exposure.
Water at \SI{48}{\celsius} will burn skin in 5 minutes, and water at \SI{59}{\celsius} will burn skin in 5 seconds.\supercite[p. 111]{nols}
Friction burns may also cause thermal burns aside from causing abrasion to the skin.

\index{burns!thermal|)}

\subsection*{Chemical Burns}
\index{burns!chemical|(}

Chemical burns are burns caused by a corrosive agent such as an acid or base.
They may also be caused by a cytotoxic agent (an agent that causes cell death) such as some types of spider venom.
Possible causes are leaking batteries, un-ignited gasoline, or strong corrosive agents used as part of an ``acid attack.''\index{acid attacks}

\index{burns!chemical|)}

\subsection*{Electrical Burns}
\index{burns!electrical|(}

Electrical burns are burns caused by electricity passing through the body.
This may be by direct contact with a live electrical object or by electricity arcing through the air.
Electrical burns cause much more damage to subcutaneous tissue than thermal burns, and in some cases there may be little observable superficial damage.
Arcing electrical shocks can cause blast injuries\index{blast injuries!electrical shock@in electrical shock} including trauma to the brain and lungs (barotrauma\index{barotrauma!electrical shock@in electrical shock}).
Burns from high voltage sources can cause respiratory and cardiac arrest.
Medics may see electrical burns from stun guns\index{stun guns!burns@in burns}, electrified fences, or downed power lines.

\index{burns!electrical|)}

\subsection*{Radiation Burns}
\index{burns!radiation|(}

Radiation burns are burns caused by radiation, or the transmission of particles or waves though a medium.
In a protest environment medics, will likely only encounter sunburns.
Burns caused by radio frequency radiation or ionizing radiation are unlikely to occur during actions.

\index{burns!radiation|)}

\section*{Burn Size}
\index{skin!burns@in burns|(}

There are two ways for estimating the \acrlong{TBSA} (\acrshort{TBSA}) that has been burned.

\index{Wallace Rule of Nines|(}
The first method is the Wallace Rule of Nines and is most accurate in adults of average body composition.
The body is divided into regions that each comprise 9\% of the \acrshort{TBSA}.
An illustration of the Wallace Rule of Nines can be seen in \autoref{fig:rule_of_nines}, and a table of the values is in \autoref{tab:rule_of_nines}.
\index{Wallace Rule of Nines|)}

\begin{figure}[htb]
\centering
\caption{Wallace Rule of Nines\supercite{anon-1}}
\label{fig:rule_of_nines}
\includesvg[width=\textwidth, height=8cm, keepaspectratio]{rule-of-nines}
\end{figure}

\begin{table}[htbp]
\centering
\caption{Wallace Rule of Nines Values}
\label{tab:rule_of_nines}
\begin{tabular}[c]{l|l}
    \multicolumn{1}{c|}{\textbf{Body Part}} &
        \multicolumn{1}{|c}{\textbf{Estimated TBSA}} \\
	\hline
    Entire left arm  & 9\%  \\
    Entire right arm & 9\%  \\
    Entire head      & 9\%  \\
    Entire chest     & 9\%  \\
    Entire abdomen   & 9\%  \\
    Entire back      & 18\% \\
    Entire left leg  & 18\% \\
    Entire right leg & 18\% \\
    Groin            & 1\%  \\
\end{tabular}
\end{table}

Another estimation technique is to use the patient's handprint with the fingers held together as 1\% of the patient's surface area.
This is reasonably accurate for both children and adults, but suffers the same deficiency as the Wallace Rule of Nines in patients with larger body proportions.

\section*{Burn Depth}

Burns are characterized by the depth of the burn.
Previous classification was to use degrees: first, second, third, and fourth.
Modern classification uses the need for surgical intervention as its basis.
This gives us new classifications: superficial, superficial partial-thickness, deep partial-thickness, full-thickness, and fourth degree.\supercite[p. 1400]{tintinallis}
The damage to the layers of skin in different burn depths can be seen in \autoref{fig:burn_depth}.
External characteristics of different burns can be seen in \autoref{fig:burn_hand_figures}.

\begin{figure}[htbp]
\caption{Burn Depth\supercite{baedr}}
\label{fig:burn_depth}
\centering
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{superficial-burn}
	\caption{Superficial Burn}
    \label{fig:superficial_burn_cross_section}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{superficial-partial-thickness-burn}
	\caption{Superficial Partial-Thickness Burn}
    \label{fig:superficial_partial_thickness_burn_cross_section}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{deep-partial-thickness-burn}
	\caption{Deep Partial-Thickness Burn}
    \label{fig:deep_partial_thickness_burn_cross_section}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{full-thickness-burn}
	\caption{Full-Thickness Burn}
    \label{fig:full_thickness_burn_cross_section}
\end{subfigure}
\end{figure}

Burns are typically not uniform in depth.
The center of a burn is likely deeper than the surrounding layers.
For example, a burn caused by heated shrapnel may be a deep partial-thickness burn in the center and be surrounded by rings of superficial partial-thickness burn and superficial burn.

\subsection*{Superficial Burn}

A superficial burn (\autoref{fig:superficial_burn_hand}) is a burn that only involves the epidermis.
The burn will be red, tender, and painful.
The wound blanches white when pressure is applied.
There is no blistering.
A common type of superficial burn is a sunburn.
Brief contact with small amounts of hot water will also cause superficial burns.

\subsection*{Superficial Partial-Thickness Burn}

A superficial partial-thickness burn (\autoref{fig:superficial_partial_thickness_burn_hand}) is a burn the involves the epidermis and the upper layer of the dermis leaving the hair follicles and glands undamaged.
The skin is red and tender like in a superficial burn.
This type of burn is the most painful.
Additionally there is blistering.
The dermis may be exposed, and if so it will be red and moist.
The burn does not extend deep enough to damage capillaries, so the skin blanches white when pressure is applied.

\subsection*{Deep Partial-Thickness Burn}

A deep partial-thickness burn (\autoref{fig:deep_partial_thickness_burn_hand}) is a burn that involves the epidermis and all layers of the dermis.
Deep structures such as hair follicles, glands, blood vessels, and nerves are all damaged.
This kind of burn may not be painful because of the damage to the nerves.
The patient may describe the pain as pressure and discomfort.
The skin does not blanch white when pressure is applied.
The exposed dermis is white to yellow in color and is fairly dry.
This type of burn is caused by exposure to flame of very hot liquids such as grease or steam.

\subsection*{Full-Thickness Burn}

% TODO mention eschar?

A full-thickness burn (\autoref{fig:full_thickness_burn_hand}) is a burn that involves the epidermis, dermis, and subcutaneous tissues.
The skin is stiff, is white to brown in color, and has a leathery texture.
The skin may be charred.
There is no pain as the nerves are completed destroyed.

\subsection*{Fourth Degree Burn}

A fourth degree burn (\autoref{fig:fourth_degree_burn_hand}) is a burn that extends all the way to the underlying bone and muscle.
Burns of this depth are catastrophically damaging.

\begin{figure}[htbp]
\caption{Burn Characteristics\supercite{baedr}}
\label{fig:burn_hand_figures}
\centering
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=4.5cm, keepaspectratio]{superficial-burn-hand}
	\caption{Superficial Burn}
    \label{fig:superficial_burn_hand}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=4.5cm, keepaspectratio]{superficial-partial-thickness-burn-hand}
	\caption{Superficial Partial-Thickness Burn}
    \label{fig:superficial_partial_thickness_burn_hand}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=4.5cm, keepaspectratio]{deep-partial-thickness-burn-hand}
	\caption{Deep Partial-Thickness Burn}
    \label{fig:deep_partial_thickness_burn_hand}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=4.5cm, keepaspectratio]{full-thickness-burn-hand}
	\caption{Full-Thickness Burn}
    \label{fig:full_thickness_burn_hand}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=4.5cm, keepaspectratio]{fourth-degree-burn-hand}
	\caption{Fourth Degree Burn}
    \label{fig:fourth_degree_burn_hand}
\end{subfigure}
\end{figure}

\index{skin!burns@in burns|)}

\section*{Burn Severity}

The severity of a burn depends on the size, depth, and location of the burn as a well as the patient's age and the presence of complicating injuries.
\autoref{tab:burn_severity} shows a method for classifying the severity of burns.
Major burns require treatment in a special burn unit.
Moderate burns require hospitalization.
Minor burns only require outpatient treatment.
Outpatient treatment may mean treatment by advanced medical care, though in some cases medics may be able to treat these injuries on their own.

\begin{table}[htbp]
\centering
\footnotesize
\caption[American Burn Association Severity Classification]
        {American Burn Association Severity Classification\supercite[p. 1400]{tintinallis}\textsuperscript{,}\footnotemark[1]}
\label{tab:burn_severity}
\begin{tabularx}{\linewidth}{|l|X|}
    \hline
    \multicolumn{1}{|c|}{\textbf{Classification}} &
        \multicolumn{1}{c|}{\textbf{Characteristics}} \\
    \hline
    \multirow{9}{*}{Major}    & Partial-thickness $>25\%$ TBSA, age 10--50          \\
                              & Partial-thickness $>20\%$ TBSA, age $<10$ or $>50$  \\
                              & Full-thickness $10\%$ TBSA                          \\
                              & Burns to hands, face, feet, or groin                \\
                              & Burns crossing major joints                         \\
                              & Circumferential burns to an extremity               \\
                              & Known inhalation injury                             \\
                              & Electrical burns                                    \\
                              & Burns complicated by fractures or other trauma      \\
    \hline
    \multirow{5}{*}{Moderate} & Partial-thickness 15--25\% TBSA, age 10--50         \\
                              & Partial-thickness 10--20\% TBSA, age $<10$ or $>50$ \\
                              & Full-thickness $\leq10\%$                           \\
                              & Suspected inhalation injury                         \\
                              & No characteristics of a major burn                  \\
    \hline
    \multirow{4}{*}{Minor}    & Partial-thickness $<15\%$ TBSA age 10--50           \\
                              & Partial-thickness $<10\%$ TBSA age $<10$ or $>50$   \\
                              & Full-thickness $<2\%$                               \\
                              & No characteristics of a major burn                  \\
    \hline
\end{tabularx}
\end{table}

\footnotetext[1]{
This table was slightly modified to remove things not applicable to medics.
}

% TODO consider including morbidity
% https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2873368/
% https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4143432/

\index{burns!circumferential|(}
% TODO what depth of burns is this relevant for?
A burn that completely wraps around an extremity is called a circumferential burn.
Circumferential burns can cause constriction that impairs circulation \gls{distal} to the burn.
Circumferential burns require advanced medical care.
\index{burns!circumferential|)}


\section*{Inhalation Injury}
\index{burns!airway@in airway|see {inhalation injury}}
\index{airway!burns@in burns|see {inhalation injury}}
\index{burns!lungs@in lungs|see {inhalation injury}}
\index{lungs!burns@in burns|see {inhalation injury}}
\index{inhalation injury|(}

Inhalation injury occurs when a patient inhales hot air, particulate matter, or toxic gasses.
Inhalation injury are most commonly seen with thermal burns, though they are also be caused by exposure irritating and toxic gasses such as cleaning agents.

Hot gasses can damage and destroy the cilia and mucous membranes of the upper (and sometimes lower) respiratory tract.
Particulate matter may reach the terminal bronchioles causing swelling and fluid to leak into the lungs.
This accumulation of fluid can cause pulmonary edema\index{pulmonary edema!inhalation injury@in inhalation injury} (accumulation of fluid in the lungs) and bronchospasm\index{bronchospasm!inhalation injury@in inhalation injury} (constricting of the muscles of airways of the lungs).
Patients with inhalation injury from thermal sources may not develop respiratory distress for 24 to 48 hours, but patients who have inhaled smoke or toxic gasses may develop immediate symptoms.

\index{carbon monoxide!poisoning|(}
Carbon monoxide has greater affinity for hemoglobin than oxygen.
When carbon monoxide is inhaled, it displaces oxygen in the hemoglobin of red blood cells, reducing their ability to deliver oxygen.
\index{pulse oximetry!carbon monoxide poisoning@and carbon monoxide poisoning|(}
This displacement cannot be detected by a pulse oximeter.
The reported \acrshort{SpO2} will be artificially high in patients with carbon monoxide poisoning, and thus pulse oximetry cannot be relied upon for diagnosis.\supercite[p. 1438]{tintinallis}
\index{pulse oximetry!carbon monoxide poisoning@and carbon monoxide poisoning|)}
\index{carbon monoxide!poisoning|)}

\index{cyanide|see {hydrogen cyanide}}
\index{hydrogen cyanide poisoning|(}
Inhaling smoke from burning wool, nylon, silk, vinyl, or plastic can lead to hydrogen cyanide poisoning.
Hydrogen cyanide poisoning causes degradation of cardiovascular, pulmonary, and central nervous system function.
Patients who complain of headache or have nausea may have inhaled toxic gasses.
\index{hydrogen cyanide poisoning|)}

\index{inhalation injury|)}

\subsection*{Signs and Symptoms}

Signs of inhalation injury are any burns to the face, singed facial or nose hair, or redness or soot in the airway.
The patient may have an irritated or constricted airway and be coughing, wheezing, or have stridor.
Stridor is a high pitched respiratory sound that is caused by turbulent air moving through constricted parts of the airway.

\index{carbon monoxide!poisoning|(}

The patient may have carbon monoxide poisoning.
Signs and symptoms are headache, nausea, vomiting, lethargy, and weakness.
Patients may also have chest pain (angina pectoris\index{angina pectoris!carbon monoxide poisoning@in carbon monoxide poisoning}), shortness of breath (dyspnea), impaired voluntary motor control (\gls{ataxia}), syncope, seizures, and coma\index{coma}.

\index{carbon monoxide!poisoning|)}

Inhalation of other toxic gasses often presents with nausea or reduced levels of consciousness.

\section*{Treatment}

There are shared treatment principles for all types of burns as well as specialized treatments for each type.

\subsection*{General Treatment}

The following steps generally apply to all burns.

\triplesubsection{Remove the patient from the source of the burn}
Treatment of the patient cannot begin until they have been safely removed from the source of burn.
This may mean removing them from a burning or smoke-filled building or getting them out of proximity of burning barricade.
When considering rescuing the patient, ensure that you do not become injured yourself as this will prevent you from being able to provide aid.

\triplesubsection{Remove jewelry and clothing}
Remove all jewelry and clothing at or near the burn site.
If patient was burned by liquid that may have accumulated in their clothing or footwear, additionally remove these garments.
Metal jewelry and accessories can retain heat and continue burning the patient.
Watches, rings, and belts can act like tourniquets and cut off circulation.
Jewelry and clothing at the burn site may trap heat or chemical agents.

\triplesubsection{Assess the burns}
After stopping the burning and checking the patient's ABCs, make an assessment on the severity of the burns.
Decide whether they need to be treated by advanced medical personnel, meaning anything above a minor burn.
Minor burns may still require advanced medical care such a burns with material melted on to the skin.

\triplesubsection{Recommend advanced medical care}
For both cosmetic reasons and to preserve functionality, patients with burns on their hands, feet, face, or groin should be promptly evacuated to advanced medical care.

\triplesubsection{Treat for shock and hypothermia}
The burns may cause the patient to go into shock.
Using water to extinguish flashes or flush chemicals from the patient may lead to hypothermia.
Treat for shock and hypothermia while awaiting evacuation.

\subsection*{Specific Treatment for Thermal Burns}
\index{burns!thermal|(}

Treatment for thermal burns begins by stopping the burning, treating life-threatening injuries, then attending to the burn itself.
Medics should not don examination gloves until after the fire is extinguished or other sources of heat are removed as the gloves may melt onto the skin of both the medic and patient.
Work gloves should be used instead.

% TODO additional warnings about dangers of approaching fire

\triplesubsection{Stop the burning}
If the patient or their clothing are still burning or smoldering, completely extinguish the fire ensuring no embers remain.
This is often accomplished simply by ``Stop, drop, and roll.''
Use a blanket, carry tarp, jacket, banner, or shirt to smother or beat the flames.
Synthetic materials such as nylon or polyester may melt into the patient's skin if used and should be avoided for large flames, though they may be appropriate for smothering or swatting small flames.
If there is a hot object such as a burning tear gas cannister or flare, you may need to use water to extinguish the source or heat.

\quadruplesubsection{Cool the burns}
After extinguishing the flame, the residual heat may continue to burn the patient.
Use cool water (18--\SI{25}{\celsius}) to quickly cool the wound.
Using cold or icy water will cause vasoconstriction which reduces blood flow to already damaged tissue.
Do not use ice as this may cause frostbite.
Be mindful that using cool water on the patient may later lead to hypothermia.

\triplesubsection{Check ABCs}
Check the patient's ABCs looking for signs of inhalation injury.
If inhalation injury is suspected, evacuate the patient to advanced medical care.
Treat life-threatening injuries while you are waiting for advanced medical personnel or before doing the evacuation yourself.

\triplesubsection{Clean and dress the burns}
Irrigate the wounds to remove debris using the steps described in \fullref{ch:wound_management}.
Do not remove material melted to the skin.
Do not pop blisters.
If a blister pops, gently pat the blister dry using gauze.
If the patient is being evacuated, cover the wounds with dry dressings.
If evacuation is delayed or the patient does not require advanced medical care, dress the wounds yourself.
Apply antiseptic or antibiotic creme to the burns.
Note that some irrigation solutions such as Prontosan may be sufficient in lieu of additional antiseptics.
\index{dressings!burn|(}
Wounds that are less than 3\% \acrshort{TBSA} can be covered with a wet dressing, otherwise use a dry dressing.
If you carry them, used specialized burn dressings, or apply burn gels to gauze.
Advise the patient to change the dressing daily until the burn heals and to seek advanced medical care if it becomes infected.
\index{dressings!burn|)}

\triplesubsection{Recommend evacuation for minor burns}
For all but the smallest burns, advise the patient to leave the action to prevent further damage, pain, and risk of infection.
If there is fire at the action, there is a good chance that riot control agents (\acrshort{RCA}s) have been or will be used.
\acrshort{RCA}s, especially pepper spray, is extremely painful on burns.

% TODO ? discuss dehydration / hypovolemic shock

\index{burns!thermal|)}

\subsection*{Specific Treatment for Chemical Burns}
\index{burns!chemical|(}

Chemical burns are caused by either dry (powder) or wet chemicals.
In treating thermal burns, you were instructed to not put on examination gloves until the flames were extinguished as the gloves could melt to your hands.
With chemical burns, you should put on gloves before removing the chemical to avoid transferring it to your body.
After removing the patient from the source of the chemical and removing their jewelry and clothing, use the following steps to treat them.

\triplesubsection{Brush off dry chemicals}
If the chemical is dry, brush off as much as possible using your hand or gauze.
Some dry chemicals will react with water.

\triplesubsection{Flush with water}
Flush the affected areas with water for at least 20 minutes.
Flushing of the eyes may need to be done for in excess of 30 minutes.
Since the most common chemical patients will become contaminated with is a form of \acrshort{RCA}, detailed instructions for decontamination procedures, especially of the eyes, can be found in \fullref{ch:rca_contamination}.
These steps likewise apply to chemical burns.
After flushing the eyes, cover them with affected eyes with a cool moist dressing.

\triplesubsection{Remove contact lenses}
If the patient is wearing contact lenses and there is any possibility that the chemical got onto their face, they will need to remove their contact lenses.
If their hands are clean and uncontaminated, they may be able to do this themself.
If not, you may need to do this for them to prevent damage to their eyes.

\index{burns!chemical|)}

\subsection*{Specific Treatment for Electrical Burns}
\index{electrical shock|seealso {burns, electrical}}
\index{electrical shock|(}
\index{burns!electrical|(}

Electrical burns may be dangerous to treat because of the invisible and unpredictable nature of electricity.
Some burns, such as those caused by a stun gun or a brief shock are safe to treat as the source of the shock is quickly removed.
Shocks caused by electric fences, exposed wiring, and especially downed power lines may make it impossible to safely rescue the patient.
To preserve your safety and thus ability to keep treating other patients, you may not be able to rescue a patient.

Wet pavement, soil, or vegetation can conduct electrical current even from low voltage lines.
This may lead to medics being shocked when approaching a patient.
Patients who come in contact with a strong enough source of electrical current may not be able to let go or get away from the source because of uncontrollable muscle contractions known as the ``no let go'' phenomenon (tetanic contractions\index{tetanic contractions!electrical shock@in electrical shock}).
If possible, use a wooden stick or bulky material without metal pieces to get the patient away from the source of shock.
However, even with these precautions, medics may be shocked.

High voltage lines that do not appear to be live may still carry current, or they may become live again after a circuit breaker flips.
This may cause the line to jump many meters.
High voltage lines have no insulation, though the presence of dirt or charred material may give this impression.
Electrical current may travel through the ground.
Wearing shoes with rubber soles may not provide sufficient insulation to prevent shock even if the shoes are rated for resistance to electrical current.
The minimum safe distance is 10 meters, and medics at this distance may still be at risk for lethal shock.

If an electrical line comes in contact with a vehicle, even after the power is cut, the vehicle may still retain charge.
A medic attempting to enter the vehicle may complete the circuit with the ground and be fatally shocked.
Likewise, patients in the vehicle should not exit the vehicle as this may complete the circuit with the ground and cause a fatal shock.

\index{electrical shock|)}

Once the patient has been removed from the source of electrical shock, check their ABCs.
Treatment principles follow that of thermal burns with the following additional considerations.
Electrical shock may cause respiratory or cardiac arrest.
Arcing electricity may create a blast wave that may injury the patient leading to brain or lung injuries.
See \fullref{ch:brain_and_spinal_cord_injuries} and \fullref{ch:chest_injuries} for more information.
Damage to muscle tissue from electrical current may lead to compartment syndrome\index{compartment syndrome!electrical shock@in electrical shock}.
Treatment for this can be found in \fullref{ch:fractures_and_dislocations}.

\index{burns!electrical|)}

\section*{Summary}

Burns can come from a variety of sources, through most commonly medics will see thermal burns from open flames.
The severity of a burn depends on its location, depth, and size.
Some minor burns may be treated by a medic in the field, but many will require advanced medical care, even if for no other reason than pain management.

Medics attempting to rescue a patient from a source of heat, chemical contaminant, or electric shock need to prioritize their own safety.
An injured medic cannot treat a patient, and their own injury becomes an additional drain on limited medical resources.

Treatment principles for burns begin with removing the source of the burn either by extinguishing flames or flushing away chemicals.
Monitoring ABCs is crucial.
Burns deeper than superficial to the hands, feet, face, or groin always require advanced medical care.
Burns to the face indicate inhalation injury which requires advanced medical care.
The skin protects the body from infection, and because a large amount of the dermis is exposed following a burn, there is high risk of infection\index{infections!burns@in burns}.
Patients who are not sent to advanced medical care need to have antibiotic creme applied to their wounds with the instructions to change their dressings daily and to seek advanced medical care if there are signs of infection.

\index{burns|)}
